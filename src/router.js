import Vue from "vue";
import Router from "vue-router";
// const home = () => import("@/pages/Home.vue");
const login = () => import("./components/Login.vue");
const event_list = () => import("./components/Events/EventManagement.vue");
const event_create = () => import("./components/Events/CreateEvent.vue");


const router = new Router({
  mode: "history",
  routes: [
    {
      path: "/",
      name: "Convert",
      component: login,
    },
    {
      path: "/event-list",
      name: "preview_resume",
      component: event_list,
    },
    {
      path: "/create-events",
      name: "preview_resume",
      component: event_create,
    },
  ],
});
router.beforeEach((to, from, next) => {
  // redirect to login page if not logged in and trying to access a restricted page
  const publicPages = ["/", "/events"];
  const authRequired = !publicPages.includes(to.path);
  const loggedIn = () => this.$session.get("token");
  if (authRequired && !loggedIn) {
    return next("/");
  }
  next();
});

export default router;
Vue.use(Router);
